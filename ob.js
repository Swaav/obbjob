




//The button you added earlier doesn't actually do anything yet; you need to attach a click handler to it using javascript:



document.getElementById("countButton").onclick = function() {
    let typedText = document.getElementById("textInput").value;
    const words = typedText.split(" ");
    
     typedText = typedText.toLowerCase();
     typedText = typedText.replace(/[^a-z'\s]+/g, "");
     const letterCounts = {};
     const WordCounts = {};

     for(let k = 0; k < words.length; k++){
         currentWord = words[k];
            if(WordCounts[currentWord] === undefined){
                WordCounts[currentWord] = 1;
            }
            else{
                WordCounts[currentWord]++;
            }
     } 
     

     for(let i = 0; i < typedText.length; i++){
         currentLetter = typedText[i];
         // your code will go here ...
        //do something for each leter.
        if(letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1;
         } 
        else {
            letterCounts[currentLetter]++;
            
        }
    }
    for (let letter in letterCounts) {
    const span = document.createElement("span");
    const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
    span.appendChild(textContent);
    document.getElementById("lettersDiv").appendChild(span);
}
   
    for(let words in WordCounts){
        const span = document.createElement("span");
        const textContent2 = document.createTextNode('"' + words + "\: " + WordCounts[words] + ", ");
        span.appendChild(textContent2);
        document.getElementById("wordsDiv").appendChild(span);
        }
}
// You also need a way to find out what was typed into the text field:
// This changes all the letter to lower case.

// Lastly, you need a way to ignore capitalization and punctuation. In a sentence like "Dogs enjoy sniffing 
// other dogs!" you want both occurrences of the word "dogs" to be counted the same, 
// even though one is capitalized and one isn't. Likewise, you don't care that one has an exclamation point and the other doesn't.

// This gets rid of all the characters except letters, spaces, and apostrophes.
// We will learn more about how to use the replace function in an upcoming lesson.
// SPIDER MAN WAS THE GREATEST AND DOC OC AINT READY 